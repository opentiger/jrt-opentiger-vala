/****************************
** Copyright © 2011 Jacques-Pascal Deplaix
**
** OpenTiger-vala is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************/

namespace Settings
{
	private const string HISTORY = "History";
	private const string BOOKMARKS = "Bookmarks";

	private const string URL = "Url";
	private const string TITLE = "Title";

	private unowned ConfigFile file;

	public void init(ConfigFile file)
	{
		Settings.file = file;
	}

	public WebKit.WebHistoryItem[] get_history()
	{
		return Settings.file.get_web_history_item_list_key(HISTORY, URL, TITLE);
	}

	public void set_history(WebKit.WebHistoryItem[] history)
	{
		Settings.file.set_web_history_item_list_key(HISTORY, URL, TITLE, history);
		Settings.file.write();
	}

	public WebKit.WebHistoryItem[] get_bookmarks()
	{
		return Settings.file.get_web_history_item_list_key(BOOKMARKS, URL, TITLE);
	}

	public void set_bookmarks(WebKit.WebHistoryItem[] bookmarks)
	{
		Settings.file.set_web_history_item_list_key(BOOKMARKS, URL, TITLE, bookmarks);
		Settings.file.write();
	}
}