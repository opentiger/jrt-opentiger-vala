/****************************
** Copyright © 2011 Jacques-Pascal Deplaix
**
** OpenTiger-vala is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************/

public class MenuBar : Gtk.MenuBar
{
	private Gtk.MenuItem item_quit = new Gtk.MenuItem.with_label("Quit");
	private Gtk.MenuItem item_history = new Gtk.MenuItem.with_label("History");
	private Gtk.MenuItem item_bookmarks = new Gtk.MenuItem.with_label("Bookmarks");
	private Gtk.MenuItem item_bookmark_this_page = new Gtk.MenuItem.with_label("Bookmark this page");
	private Gtk.MenuItem item_about = new Gtk.MenuItem.with_label("About");
	private Gtk.MenuItem item_clear_url = new Gtk.MenuItem.with_label("ClearURL");

	public signal void quit_clicked();
	public signal void history_clicked();
	public signal void bookmarks_clicked();
	public signal void about_clicked();
	public signal void clear_url_clicked();
	public signal void bookmark_this_page_clicked();

	public MenuBar()
	{
		var menu_file = new MenuItem("File", {
				this.item_quit
			});

		var menu_history = new MenuItem("History", {
				this.item_history
			});

		var menu_bookmarks = new MenuItem("Bookmarks", {
				this.item_bookmark_this_page,
				this.item_bookmarks
			});

		var menu_help = new MenuItem("Help", {
				this.item_about
			});

		var menu_clear_url = new MenuItem("ClearURL",{
				this.item_clear_url
			});

		this.append(menu_file);
		this.append(menu_history);
		this.append(menu_bookmarks);
		this.append(menu_clear_url);
		this.append(menu_help);

		this.item_quit.activate.connect(() => this.quit_clicked());
		this.item_history.activate.connect(() => this.history_clicked());
		this.item_bookmarks.activate.connect(() => this.bookmarks_clicked());
		this.item_about.activate.connect(() => this.about_clicked());
		this.item_clear_url.select.connect(() => this.clear_url_clicked());
		this.item_bookmark_this_page.activate.connect(() => this.bookmark_this_page_clicked());
	}
}
