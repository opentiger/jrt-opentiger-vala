/****************************
** Copyright © 2011 Jacques-Pascal Deplaix
**
** OpenTiger-vala is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************/

public class TreeDialog : Gtk.Dialog
{
	private Gtk.TreeView tree = new Gtk.TreeView();
	protected Gtk.ListStore store = new Gtk.ListStore(1, typeof(string));

	public TreeDialog()
	{
		this.tree.model = this.store;

		this.add_buttons(Gtk.Stock.OK, Gtk.ResponseType.OK,
						 Gtk.Stock.CANCEL, Gtk.ResponseType.CANCEL);

		this.tree.insert_column_with_attributes(-1, "Url", new Gtk.CellRendererText(), "text", 0);

		var main_box = (Gtk.Box)(this.get_content_area());
		main_box.pack_start(this.tree);

		this.tree.show();
	}
}