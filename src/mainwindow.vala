/****************************
** Copyright © 2011 Jacques-Pascal Deplaix
**
** OpenTiger-vala is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************/

public class MainWindow : Gtk.Window
{
	private MenuBar menubar = new MenuBar();
	private Toolbar toolbar = new Toolbar();
	private Notebook notebook = new Notebook();
	private Statusbar statusbar = new Statusbar();

	public MainWindow()
	{
		var main_box = new Gtk.VBox(false, 0);
		main_box.pack_start(this.menubar, false);
		main_box.pack_start(this.toolbar, false);
		main_box.pack_start(this.notebook);
		main_box.pack_start(this.statusbar, false);

		this.destroy.connect(Gtk.main_quit);
		this.menubar.quit_clicked.connect(Gtk.main_quit);
		this.menubar.history_clicked.connect(() => {
				var window = new History();
				window.run();
				window.destroy();
			});
		this.menubar.bookmarks_clicked.connect(() => {
				var window = new Bookmarks();
				window.run();
				window.destroy();
			});
		this.menubar.bookmark_this_page_clicked.connect(() => {
				var bookmarks = Settings.get_bookmarks();
				bookmarks += new WebKit.WebHistoryItem.with_data(this.notebook.current_page.uri, this.notebook.current_page.title);

				Settings.set_bookmarks(bookmarks);
			});
		this.menubar.about_clicked.connect(() => {
				var window = new About(this);
				window.run();
				window.destroy();
			});
		this.menubar.clear_url_clicked.connect(() => {
				this.toolbar.set_url("");
			});
		this.notebook.title_changed.connect(this.set_title);
		this.notebook.load_finished.connect(this.toolbar.set_url);
		this.notebook.can_go_back.connect(this.toolbar.set_back_sensitive);
		this.notebook.can_go_forward.connect(this.toolbar.set_forward_sensitive);
		this.notebook.hovering_over_link.connect((url) => this.statusbar.push_url(url));
		this.toolbar.back_clicked.connect(this.notebook.go_back);
		this.toolbar.forward_clicked.connect(this.notebook.go_forward);
		this.toolbar.reload_clicked.connect(this.notebook.reload);
		this.toolbar.entry_activate.connect(this.notebook.load);

		this.add(main_box);
	}

	public void display()
	{
		this.maximize();
		this.show_all();
	}
}
