/****************************
** Copyright © 2011 Jacques-Pascal Deplaix
**
** OpenTiger-vala is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************/

public class Notebook : Gtk.Notebook
{
	private Gtk.Button button_add = new Gtk.Button();

	public signal void title_changed(string title);
	public signal void load_finished(string uri);
	public signal void can_go_back(bool go_back);
	public signal void can_go_forward(bool go_forward);
	public signal void hovering_over_link(string? url);

	public WebView current_page
	{
		get
		{
			return (WebView)(((Gtk.ScrolledWindow)(this.get_nth_page(this.get_current_page()))).child);
		}
	}

	public Notebook()
	{
		this.button_add.image = new Gtk.Image.from_stock(Gtk.Stock.ADD,
														 Gtk.IconSize.SMALL_TOOLBAR);
		this.button_add.relief = Gtk.ReliefStyle.NONE;
		this.button_add.show();
		this.button_add.clicked.connect(this.add_page);
		this.set_action_widget(this.button_add, Gtk.PackType.END);

		this.add_page();
	}

	private void add_page()
	{
		var webview = new WebView();

		var scrolled_window = new Gtk.ScrolledWindow(null, null);
        scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC);
        scrolled_window.add(webview);

		var hbox = new Gtk.HBox(false, 0);
		var label = new Gtk.Label(null);
		var button = new Gtk.Button();
		button.image = new Gtk.Image.from_stock(Gtk.Stock.CLOSE, Gtk.IconSize.MENU);
		button.relief = Gtk.ReliefStyle.NONE;
		button.can_default = false;
		button.can_focus = false;

		Gtk.RcStyle style = new Gtk.RcStyle();
		style.xthickness = style.ythickness = 0;
		button.modify_style(style);

		button.style_set.connect(() =>
		{
			int wi, hi;
			Gtk.icon_size_lookup_for_settings(button.image.get_settings(), Gtk.IconSize.MENU, out wi, out hi);
			button.set_size_request(wi, hi);
		});

		hbox.pack_start(label);
		hbox.pack_start(button);

		int tab_index = this.append_page(scrolled_window, hbox);

		button.clicked.connect(() => this.remove_page(tab_index));
		webview.title_changed.connect((a, title) => label.label = title);
		webview.title_changed.connect((a, title) => this.title_changed(title));
		webview.load_committed.connect((fram) => this.load_finished(fram.uri));
		webview.load_committed.connect(() => this.can_go_back(webview.can_go_back()));
		webview.load_committed.connect(() => this.can_go_forward(webview.can_go_forward()));
		webview.hovering_over_link.connect((a, url) => this.hovering_over_link(url));

		label.show_all();
		scrolled_window.show_all();
		button.show_all();
	}

	public void go_back()
	{
		this.current_page.go_back();
	}

	public void go_forward()
	{
		this.current_page.go_forward();
	}

	public void load(string url)
	{
		this.current_page.load_uri(url);
	}

	public void reload()
	{
		this.current_page.reload();
	}
}