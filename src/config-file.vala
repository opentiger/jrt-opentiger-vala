/****************************
** Copyright © 2011 Jacques-Pascal Deplaix
**
** OpenTiger-vala is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************/

public class ConfigFile : GLib.KeyFile
{
	public ConfigFile()
	{
		try
		{
			this.load_from_file(this.filename(), GLib.KeyFileFlags.NONE);
		}
		catch(GLib.Error error)
		{
		}
	}

	public void write()
	{
		try
		{
			GLib.FileUtils.set_contents(this.filename(), this.to_data());
		}
		catch(GLib.Error error)
		{
		}
	}

	public string[] get_string_list_key(string group, string key)
	{
		try
		{
			return this.get_string_list(group, key);
		}
		catch(GLib.Error error)
		{
		}

		return {};
	}

	public WebKit.WebHistoryItem[] get_web_history_item_list_key(string group, string key1, string key2)
	{
		var url = this.get_string_list_key(group, key1);
		var title = this.get_string_list_key(group, key2);
		var value = new WebKit.WebHistoryItem[0];

		for(size_t i = 0; i < url.length; ++i)
		{
			value += new WebKit.WebHistoryItem.with_data(url[i], title[i]);
		}

		return value;
	}

	public void set_web_history_item_list_key(string group, string key1, string key2, WebKit.WebHistoryItem[] history)
	{
		var url = new string[0];
		var title = new string[0];

		foreach(var item in history)
		{
			url += item.uri;
			title += item.title;
		}

		Settings.file.set_string_list(group, key1, url);
		Settings.file.set_string_list(group, key2, title);
	}

	private string filename()
	{
		string path = GLib.Environment.get_user_config_dir() + "/opentiger-vala/";
		this.verify_dir(path);

		string file = path + "config.ini";
		this.verify_file(file);

		return file;
	}

	private void verify_dir(string dir)
	{
		if(!GLib.FileUtils.test(dir, GLib.FileTest.EXISTS))
		{
			// Create the dir
			GLib.DirUtils.create(dir, 0700);
		}
	}

	private void verify_file(string file)
	{
		if(!GLib.FileUtils.test(file, GLib.FileTest.EXISTS))
		{
			// Create the file
			GLib.FileStream.open(file, "w");
		}
	}
}