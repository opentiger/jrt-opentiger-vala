/****************************
** Copyright © 2011 Jacques-Pascal Deplaix
**
** OpenTiger-vala is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************/

public class Toolbar : Gtk.Toolbar
{
	private Gtk.ToolButton back = new Gtk.ToolButton.from_stock(Gtk.Stock.GO_BACK);
	private Gtk.ToolButton forward = new Gtk.ToolButton.from_stock(Gtk.Stock.GO_FORWARD);
	private Gtk.ToolButton reload = new Gtk.ToolButton.from_stock(Gtk.Stock.REFRESH);
	private Gtk.Entry entry = new Gtk.Entry();

	public signal void back_clicked();
	public signal void forward_clicked();
	public signal void reload_clicked();
	public signal void entry_activate(string url);

	public Toolbar()
	{
		this.toolbar_style = Gtk.ToolbarStyle.ICONS;

		var item_entry = new Gtk.ToolItem();
		item_entry.add(this.entry);
		item_entry.set_expand(true);

		this.add(this.back);
		this.add(this.forward);
		this.add(this.reload);
		this.add(item_entry);

		this.back.clicked.connect(() => this.back_clicked());
		this.forward.clicked.connect(() => this.forward_clicked());
		this.reload.clicked.connect(() => this.reload_clicked());
		this.entry.activate.connect(() => this.entry_activate(this.entry.text));
	}

	public void set_url(string url)
	{
		this.entry.text = url;
	}

	public void set_back_sensitive(bool sensitive)
	{
		this.back.sensitive = sensitive;
	}

	public void set_forward_sensitive(bool sensitive)
	{
		this.forward.sensitive = sensitive;
	}
}