/****************************
** Copyright © 2011 Jacques-Pascal Deplaix
**
** OpenTiger-vala is free software: you can redistribute it and/or modify
** it under the terms of the GNU General Public License as published by
** the Free Software Foundation, either version 3 of the License, or
** (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program.  If not, see <http://www.gnu.org/licenses/>.
****************************/

public class WebView : WebKit.WebView
{
	public WebView()
	{
		this.load_uri("http://www.google.fr/");

		this.load_committed.connect(this.add_history);
	}

	private void add_history()
	{
		var items = Settings.get_history();

		if(this.uri != "")
			items += new WebKit.WebHistoryItem.with_data(this.uri, this.title);

		Settings.set_history(items);
	}
}