#! /usr/bin/env python
# encoding: utf-8
# Copyright © 2011 Jacques-Pascal Deplaix

APPNAME = 'opentiger-vala'
VERSION = '0.1'

top = '.'
out = 'build'

def options(opt):
	opt.load(['compiler_c', 'vala'])

def configure(conf):
	conf.env.CFLAGS = list()
	conf.env.VALAFLAGS = list()

	conf.load(['compiler_c', 'vala'])

	conf.check_cfg(
		package         = 'glib-2.0',
		uselib_store    = 'GLIB',
		atleast_version = '2.14.0',
		args            = ['--cflags', '--libs'])

	conf.check_cfg(
		package         = 'gobject-2.0',
		uselib_store    = 'GOBJECT',
		atleast_version = '2.14.0',
		args            = ['--cflags', '--libs'])

	conf.check_cfg(
		package         = 'gthread-2.0',
		uselib_store    = 'GTHREAD',
		atleast_version = '2.14.0',
		args            = ['--cflags', '--libs'])

	conf.check_cfg(
		package         = 'gtk+-2.0',
		uselib_store    = 'GTK',
		atleast_version = '2.16',
		args            = ['--cflags', '--libs'])

	conf.check_cfg(
		package         = 'webkit-1.0',
		uselib_store    = 'WEBKIT',
		atleast_version = '1.0',
		args            = ['--cflags', '--libs'])

	conf.env.CFLAGS.extend(['-pipe', '-I/usr/local/include', '-include', 'config.h', '-g'])
	conf.env.VALAFLAGS.extend(['--fatal-warnings', '-g'])
	conf.define('VERSION', VERSION)

	conf.write_config_header('config.h')

def build(bld):
	bld.program(
		packages  = ['webkit-1.0', 'config'],
		target    = APPNAME,
		vapi_dirs = 'vapi',
		uselib    = ['GLIB', 'GOBJECT', 'GTHREAD', 'GTK', 'WEBKIT'],
		source    = ['src/about.vala',
			     'src/bookmarks.vala',
			     'src/config-file.vala',
			     'src/history.vala',
			     'src/main.vala',
			     'src/mainwindow.vala',
			     'src/menubar.vala',
			     'src/menu-item.vala',
			     'src/notebook.vala',
			     'src/settings.vala',
			     'src/statusbar.vala',
			     'src/toolbar.vala',
			     'src/tree-dialog.vala',
			     'src/webview.vala'])
